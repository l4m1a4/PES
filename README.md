LICENSE: READ BEFORE USE

Written permissions from me (phamtronglam2001@gmail.com) is required before you can use the output figures onto your research publications.
Please repsect the license.
I am not responsible for the potential complications regarding authorship issues to you publications if you do not respect this LICENSE.


Usage: Make sure POSCAR.vasp, Exy data file (txt) are present
Run python PES.py
Edit plot.ini
Run python PES.py
Use the exported figures for your publication!


Note: in Python3, ConfigParser becomes configparser. 
This code is only compatible with ASE up to version 3.17.0 because later versions of ASE require Python 3 and this code breaks!


phamtronglam2001@gmail.com
This PES (Potential Energy Surface) is built to have the following features:
1. Draw lattice (automatically add boundary atoms), automatically remove adsorbate (by setting cutoff in plot.ini) 
2. Draw Exy scan (related to a tool in the research group)
3. Plot overlay Exy and lattice


To plot lattice separately (using ASE Atomic Simulation Environment), 
first replace utils.py with the corresponding file in the folder.
and use 

draw_by_ase = True in plot.ini

Hidden function to plot POSCAR

python PES_5.py POSCARfile.vasp

exported PDB file is equipped with additonal atoms at boundaries for better visualization.

Example outputs

![image](plot_Silicene_Acetone_revPBE-demo.png)
![image](plot_Silicene_Acetone_revPBE-demo.png.adsorbate.png)
![image](plot_Silicene_Acetone_revPBE-demo.png.ase_style.png)
![image](plot_Silicene_Acetone_revPBE-demo.png.ase_style_substrate.png)
![image](plot_Silicene_Acetone_revPBE-demo.png.heatmap_adsorbate.png)
![image](plot_Silicene_Acetone_revPBE-demo.png.substrate.png)
![image](Si-acetone-vacancy.png)
