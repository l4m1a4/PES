echo Usage: plot_PES.sh Energy.dat_file POSCAR_file title
echo Example: plot_PES.sh xyz0 00-POSCAR_xy Si-acetonitrile
echo ==============================

echo Plotting Exy PES with 
echo data = $1
echo POSCAR = "$2".vasp
cp $2 "$2".vasp
echo title = $3
echo Creating plot.ini ...
python ~/bin/PES_5.4.py >> /dev/null
sed -i -e "5c data_file = $1" plot.ini
sed -i -e "11c substrate_file = "$2".vasp" plot.ini
sed -i -e "15c plt_title = $3" plot.ini
sed -i -e "19c plt_filename = "$3".png" plot.ini  
echo Plotting ...
python ~/bin/PES_5.4.py
echo DONE
