# -*- coding: utf-8 -*-
"""
Created on Thu Aug  30 18:49:30 2018

@author: phamtronglam2001@gmail
"""
from __future__ import print_function
import sys
import math
import numpy as np
import matplotlib
# Turn on xkcd style here
#matplotlib.pyplot.xkcd(scale=4, length=100, randomness=5)
#from scipy.interpolate import griddata
#    xi = np.linspace(X.min(),X.max(),1000)
#    yi = np.linspace(Y.min(),Y.max(),1000)
#    zi = griddata((X, Y), Z, (xi[None,:], yi[:,None]), method='cubic')
#    CS = ax.contourf(xi,yi,zi, 10, cmap=plt.cm.rainbow)
# end of xkcd style
###################
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker
#from scipy.interpolate import griddata
import ase
from ase import Atoms
#from ase import Atom
from ase.io import read,write
import ase.build
from ase.visualize.plot import plot_atoms
# Require patching of ase.io.utils in the same folder)
#plot_atoms(substrate,ax=ax,rotation=('0x,0y,0z'),show_unit_cell=True,offset=offset,radii=radii,colors=colors)
from AddAtomsAtBoundary import *

########################################
### CONFIG file
#from vars import *
import sys
import ConfigParser
import io
import os
from datetime import datetime

pes_version = "5.2"
configfile_name = "plot.ini"
log_filename = configfile_name + '.' + datetime.now().strftime('%Y_%B_%d_%H-%M-%S')
 
def exit_prog():
    print('\n\n')
    print( "IF ERROR OCCURS AND YOU DON'T KNOW WHAT ELSE TO DO, THEN DELETE " + configfile_name + " AND RERUN THE PROGRAM!")
    print( "IF ALL ELSE FAILED, YOU MAY CONTACT THE DEVELOPER at phamtronglam2001@gmail.com")
#    print "In that case, please set your email title understandable, attach all necessary files to reproduce the problem, and expect reply no sooner than one week"
#    print "Please be patient, probably the developer is busy doing his master thesis"
    print( 'Program ended at: ' + str(datetime.now()))
    sys.exit(1)
    pass

def write_config_file(): 
    # Create the configuration file as it doesn't exist yet
    cfgfile = open(configfile_name, 'w')

    # Add content to the file
    Config = ConfigParser.ConfigParser(allow_no_value=True)
    Config.add_section('NORMAL')
    Config.set('NORMAL', '; ATTENTION: ALL UNITS ARE IN ANGSTROMS!!!!')
    Config.set('NORMAL','pes_version',pes_version)
    Config.set('NORMAL', '; file containing X, Y and Energy columns')
    Config.set('NORMAL', 'data_file', 'dat.txt')
    Config.set('NORMAL', '; index of X, Y, and Energy column, respectively')
    Config.set('NORMAL', '; ATTENTION: index starts from zero (0 means first column)')
    Config.set('NORMAL', 'xyzcolumns_index', '0,1,4')
    Config.set('NORMAL', '; POSCAR file (if adsorbate is included, please check z_cut_downline and z_cut_upline also!')
    Config.set('NORMAL', '; Filename must be in the form *.vasp')
    Config.set('NORMAL', 'substrate_file', 'POSCAR_z1.vasp')
    Config.set('NORMAL', '; Number of replication times along each axis')
    Config.set('NORMAL', 'data_replication_x', 4)
    Config.set('NORMAL', 'data_replication_y', 4)
    Config.set('NORMAL', 'plt_title', 'Projected Potential Energy Surface (revPBE) for Silicene-Acetone $C_3H_6O$')
    Config.set('NORMAL', 'plt_xlabel',r'X ($\AA$)')
    Config.set('NORMAL', 'plt_ylabel',r'Y ($\AA$)')
    Config.set('NORMAL', '; PNG is recommened')
    Config.set('NORMAL', 'plt_filename','plot_Silicene_Acetone_revPBE-demo.png')
    Config.set('NORMAL', 'plt_dpi',600)
    
    Config.add_section('PLOT')
    Config.set('PLOT', '; Number of lines to skip at the beginning of data_file')
    Config.set('PLOT','header_lines',1)
    Config.set('PLOT', '; Atoms below this height (Angstrom) will NOT be drawn')
    Config.set('PLOT','z_cut_downline',0) 
    Config.set('PLOT', '; Atoms above this height (Angstrom) will NOT be drawn')
    Config.set('PLOT','z_cut_upline',2.5) 
    Config.set('PLOT', '; color of atoms circle')
    Config.set('PLOT','r_linecolor','black')
    Config.set('PLOT', '; radius of atoms')
    Config.set('PLOT','r_circle',0.45)
    Config.set('PLOT', '; radius cut of bonds')    
    Config.set('PLOT','r_line',0.5)   
    Config.set('PLOT', '; color of bonds')    
    Config.set('PLOT','r_bondcolor','black')      
    Config.set('PLOT', '; transparency of atoms, value between 0 and 1')
    Config.set('PLOT','transparency',0.4)  
    Config.set('PLOT', '; atoms within this distance will be bonded (Angstrom)')
    Config.set('PLOT','d_cutoff',3)
    Config.set('PLOT', '; subtrate atom above this height will be colored differently')
    Config.set('PLOT','z_cutoff',0.1) 
    Config.set('PLOT', '; color of upper layer atoms')
    Config.set('PLOT','r_color','red')   
    Config.set('PLOT', '; Margin of cell plot')
    Config.set('PLOT','left_margin',0)  
    Config.set('PLOT','right_margin',0)  
    Config.set('PLOT','top_margin',0)  
    Config.set('PLOT','bottom_margin',0)  
 
    Config.add_section('DEVELOPER')    
    Config.set('DEVELOPER','; Size of polygon around cell, used to hide excess PES data')
    Config.set('DEVELOPER','extent',15)
    Config.set('DEVELOPER','; Polygon visibility')
    Config.set('DEVELOPER','invisibility',1)
    Config.set('DEVELOPER','; Polygon color')
    Config.set('DEVELOPER','polygon_color','white')    
    Config.set('DEVELOPER','cbar_label',r'meV')
    Config.set('DEVELOPER','; Number of levels between min E and max E')
    Config.set('DEVELOPER','fineness',100)
    Config.set('DEVELOPER','cbar_orientation','vertical')
    Config.set('DEVELOPER','; Control heatmap cbar size etc')
    Config.set('DEVELOPER','cbar_shrink',2)
    Config.set('DEVELOPER','cbar_pad',0.05)
    Config.set('DEVELOPER','cbar_aspect',15)
    Config.set('DEVELOPER','cbar_fraction',0.03)
    Config.set('DEVELOPER','cbar_tick_labelsize',10)
    Config.set('DEVELOPER','cbar_font_style','italic')
    Config.set('DEVELOPER','cbar_font_size',16)
    Config.set('DEVELOPER','cbar_font_stretch','expanded')
    Config.set('DEVELOPER','cbar_label_pad',10)
    Config.set('DEVELOPER','cbar_label_y',0.5)
    Config.set('DEVELOPER','; Control heatmap colorbar levels')
    Config.set('DEVELOPER','cbar_level_override',False)
    Config.set('DEVELOPER','cbar_level_linspace_start',0)
    Config.set('DEVELOPER','cbar_level_linspace_end',40)
    Config.set('DEVELOPER','cbar_level_linspace_segment',5)
    Config.set('DEVELOPER','cbar_level_manual',False)
    Config.set('DEVELOPER','cbar_levels','0,20,40,60,80')
    Config.set('DEVELOPER','; plot_style: 3 options, xkcd (This is FUN), seaborn, or matplotlib')
    Config.set('DEVELOPER','plot_style','matplotlib')
    Config.set('DEVELOPER','; Hide/show polygon around lattice')
    Config.set('DEVELOPER','draw_polygon',True)
    Config.set('DEVELOPER','; Show Atoms drawn in ASE style (need to adjust offset and modify ase.io.utils, see sourcecode for instructions)')
    Config.set('DEVELOPER','draw_by_ase',True)
    Config.set('DEVELOPER','offset','0, 0')
    Config.set('DEVELOPER','ase_background_color','xkcd:mint green')
    Config.set('DEVELOPER','ase_background_alpha',1)
    Config.set('DEVELOPER','ase_atom_alpha',0.7)
    Config.write(cfgfile)
    cfgfile.close()
    pass

def load_parameters():
    # Read in values if file exists
    try:
        with open(configfile_name) as f:
            sample_config = f.read()
    except IOError as e:
        print( "I/O error({0}): {1}".format(e.errno, e.strerror))
    except: #handle other exceptions such as attribute errors
        print( "Unexpected error:", sys.exc_info()[0])
        exit_prog()
    
    config = ConfigParser.RawConfigParser(allow_no_value=True)
    config.readfp(io.BytesIO(sample_config))
    f.close()
    
    # Compare INI_version and pes_version, and do something
    global ini_version
    ini_version = float(config.get('NORMAL','pes_version'))
    
    global data_file,xyzcolumns_index,substrate_file,data_replication_x,data_replication_y
    global plt_title,plt_xlabel,plt_ylabel,plt_filename,plt_dpi
    
    data_file = config.get('NORMAL','data_file')
    xyzcolumns_index = config.get('NORMAL',
                                  'xyzcolumns_index').split(',')
    xyzcolumns_index = [int(i) for i in xyzcolumns_index]
    substrate_file = config.get('NORMAL','substrate_file')
    data_replication_x = int(config.get('NORMAL','data_replication_x'))
    data_replication_y = int(config.get('NORMAL','data_replication_y'))
    plt_title = config.get('NORMAL','plt_title')
    plt_xlabel = config.get('NORMAL','plt_xlabel')
    plt_ylabel = config.get('NORMAL','plt_ylabel')
    plt_filename = config.get('NORMAL','plt_filename')
    plt_dpi = int(config.get('NORMAL','plt_dpi'))   
    
    global header_lines,z_cut_downline,z_cut_upline,r_color,r_linecolor,r_circle
    global r_bondcolor, r_line,transparency,d_cutoff,z_cutoff
    global left_margin,right_margin,top_margin,bottom_margin
    header_lines = int(config.get('PLOT','header_lines'))
    z_cut_downline = float(config.get('PLOT','z_cut_downline'))
    z_cut_upline = float(config.get('PLOT','z_cut_upline'))
    r_color = config.get('PLOT','r_color')
    r_linecolor = config.get('PLOT','r_linecolor')
    r_circle = float(config.get('PLOT','r_circle'))
    r_bondcolor=config.get('PLOT','r_bondcolor')
    r_line = float(config.get('PLOT','r_line'))
    transparency = float(config.get('PLOT','transparency'))
    d_cutoff = float(config.get('PLOT','d_cutoff'))
    z_cutoff = float(config.get('PLOT','z_cutoff'))
    left_margin = float(config.get('PLOT','left_margin'))
    right_margin = float(config.get('PLOT','right_margin'))
    top_margin = float(config.get('PLOT','top_margin'))
    bottom_margin = float(config.get('PLOT','bottom_margin'))
    
    global draw_polygon,invisibility,polygon_color,extent,plot_style,cbar_label
    global fineness,cbar_orientation,cbar_shrink,cbar_pad,cbar_aspect,cbar_fraction
    global cbar_tick_labelsize,cbar_font_style,cbar_font_size,cbar_font_stretch
    global cbar_label_pad,cbar_label_y,cbar_level_override,cbar_level_linspace_start
    global cbar_level_linspace_segment,cbar_level_linspace_end,cbar_level_manual
    global cbar_levels,draw_by_ase,offset
    
    draw_polygon = config.getboolean('DEVELOPER','draw_polygon')
    invisibility = float(config.get('DEVELOPER','invisibility'))
    polygon_color = config.get('DEVELOPER','polygon_color')
    extent = float(config.get('DEVELOPER','extent'))
    plot_style=config.get('DEVELOPER','plot_style')
    cbar_label=config.get('DEVELOPER','cbar_label')
    fineness = float(config.get('DEVELOPER','fineness'))
    cbar_orientation = config.get('DEVELOPER','cbar_orientation')
    cbar_shrink = float(config.get('DEVELOPER','cbar_shrink'))
    cbar_pad = float(config.get('DEVELOPER','cbar_pad'))
    cbar_aspect = float(config.get('DEVELOPER','cbar_aspect'))
    cbar_fraction = float(config.get('DEVELOPER','cbar_fraction'))
    cbar_tick_labelsize = float(config.get('DEVELOPER','cbar_tick_labelsize'))
    cbar_font_style = config.get('DEVELOPER','cbar_font_style')
    cbar_font_size = float(config.get('DEVELOPER','cbar_font_size'))
    cbar_font_stretch = config.get('DEVELOPER','cbar_font_stretch')
    cbar_label_pad = float(config.get('DEVELOPER','cbar_label_pad'))
    cbar_label_y = float(config.get('DEVELOPER','cbar_label_y'))
    cbar_level_override = config.getboolean('DEVELOPER','cbar_level_override')
    cbar_level_linspace_start = float(config.get('DEVELOPER','cbar_level_linspace_start'))
    cbar_level_linspace_end = float(config.get('DEVELOPER','cbar_level_linspace_end'))
    cbar_level_linspace_segment = float(config.get('DEVELOPER','cbar_level_linspace_segment'))
    cbar_level_manual = config.getboolean('DEVELOPER','cbar_level_manual')
    cbar_levels = config.get('DEVELOPER','cbar_levels').split(',')
    cbar_levels = [int(i) for i in cbar_levels]
    draw_by_ase = config.getboolean('DEVELOPER','draw_by_ase')
    offset = config.get('DEVELOPER','offset').split(',')
    offset = [float(i) for i in offset]
    
    global ase_background_color,ase_background_alpha,ase_atom_alpha
    ase_background_color = config.get('DEVELOPER','ase_background_color')
    ase_background_alpha = float(config.get('DEVELOPER','ase_background_alpha'))
    ase_atom_alpha = float(config.get('DEVELOPER','ase_atom_alpha'))
    
    #save config to another file
    print( 'Saving parameters to ', log_filename, ' ...')
    f= open(log_filename,'w+')
    f.write(sample_config)
    f.close()
    print( 'Saving done.')
  
    pass

def load_config_file():
    # Check if there is already a configurtion file
    if not os.path.isfile(configfile_name):
        print( 'Config file not FOUND!')
        print( 'Generating ' + configfile_name + ' file. Please wait ...')
        write_config_file()
        print( 'Writing to ' + configfile_name + ' done!')
        print( 'Please edit ' + configfile_name + ' with the appropriate content')
        print( 'Then run again the software')
        exit_prog()
    	
    if os.path.isfile(configfile_name):
        print( 'Found config file! Loading parameters ...')
        load_parameters()
        print( 'Done loading parameters')
        
    pass


########################################
######################################################
print( 'Loading data')

def load_substrate():
    try:
        global substrate
        substrate=ase.build.sort(read(substrate_file))
    except: #handle other exceptions such as attribute errors
        print( 'ERROR OCCURRED: Please check the subtrate_file in ' + configfile_name)
        print( 'substrate_file = ' + substrate_file)
        exit_prog()
    global Transform_matrix
    #Transform_matrix=[    [15.5609989165999991,    0.0000000000000000,    0.0000000000000000],
    #    [-7.7804994582999996,   13.4762203701000001,    0.0000000000000000],
    #     [0.0000000000000000,    0.0000000000000000,   20.1595039368000002]]
    Transform_matrix=substrate.get_cell()
    pass

def load_energy():
    # Load data from CSV
    try:
        dat = np.genfromtxt(data_file, dtype=np.str,skip_header=header_lines)
    except:
        print( 'ERROR OCCURRED: Please check the data_file in ' + configfile_name)
        print( 'data_file = ' + data_file)
        exit_prog()    
    #print dat[0,0]
    X_dat = dat[:,xyzcolumns_index[0]].astype(np.float64)
    Y_dat = dat[:,xyzcolumns_index[1]].astype(np.float64)
    Z_dat = dat[:,xyzcolumns_index[2]].astype(np.float64)    
    # Convert from pandas dataframes to numpy arrays
    global X1, Y1, Z1
    X1, Y1, Z1 = np.array([]), np.array([]), np.array([])
    for i in range(len(X_dat)):
            X1 = np.append(X1,X_dat[i])
            Y1 = np.append(Y1,Y_dat[i])
            Z1= np.append(Z1,Z_dat[i])
    pass
###########

def replicate_data():
    ########### 
    # Replicate data data_replication_x and data_replication_y
    #data_replication_x, data_replication_y=4,4
    
    # convert eV to meV scale
    unit_multiplication=1000        
    global Z1
    Z1= np.interp(Z1, (Z1.min(), Z1.max()), (0, unit_multiplication*(Z1.max()-Z1.min())))
    
    global X,Y,Z
    X,Y,Z=X1,Y1,Z1
    #
    for i in range(-2*data_replication_x,2*data_replication_x):
        X = np.concatenate((X,X1+i*(1./data_replication_x)),axis=0)
        Y = np.concatenate((Y,Y1),axis=0)
        Z =  np.concatenate((Z,Z1),axis=0)
        
    for i in range(-data_replication_y,data_replication_y):
        X = np.concatenate((X,X),axis=0)
        Y = np.concatenate((Y,Y+i*(1./data_replication_y)),axis=0)
        Z =  np.concatenate((Z,Z),axis=0)
    pass
        
####################
def transform_data():
    ####################
    # Transform data
    #print 'Lattice cell matrix:'
    #print Transform_matrix
    global X,Y
    X = X*Transform_matrix[0][0]+Y*Transform_matrix[1][0]
    Y = X*Transform_matrix[0][1]+Y*Transform_matrix[1][1]
    pass
#################

########################################
# Draw heat map
def plot_heatmap():
    print( 'Plotting ....')
    global fig, ax
    fig, ax = plt.subplots()
    ax.set_aspect('equal')
    
    
    CS = ax.tricontourf(X,Y,Z, np.linspace(min(Z),max(Z),fineness), cmap=plt.cm.gnuplot)
#    print 'cbar_level_override = ',cbar_level_override
#    print 'cbar_level_manual = ',cbar_level_manual
    if cbar_level_override==True and cbar_level_manual==False:
        cbar = plt.colorbar(CS, ticks=np.sort(np.array(np.linspace(cbar_level_linspace_start,
                        cbar_level_linspace_end,cbar_level_linspace_segment))),
                        ax=ax, orientation=cbar_orientation,
                        shrink=cbar_shrink, pad=cbar_pad, aspect=cbar_aspect,fraction=cbar_fraction)
    if cbar_level_override==False and cbar_level_manual==False:
        print("max(Z)=", max(Z))
        print ("round(int(max(Z)))=",round(int(max(Z))))
        
        if max(Z)>10.:
            a = np.linspace(0,round(int(max(Z))/10)*10,5)
        else:
            a = np.linspace(0,round(int(max(Z))),5)
        print ("cbar ticks = ", a)
        cbar = plt.colorbar(CS, ticks=np.sort(a),
                        ax=ax, orientation=cbar_orientation,
                        shrink=cbar_shrink, pad=cbar_pad, aspect=cbar_aspect,fraction=cbar_fraction)
    if cbar_level_manual==True:
        print ('cbar_levels = ',cbar_levels)
        #levels = [0, 10, 20, 30, 40, 50]
        cbar = plt.colorbar(CS, ticks=np.sort(np.sort(np.array(cbar_levels))),
                        ax=ax, orientation=cbar_orientation,
                        shrink=cbar_shrink, pad=cbar_pad, aspect=cbar_aspect,fraction=cbar_fraction)
        #cbar.ax.set_yticklabels(list(map(str,np.sort(np.array(cbar_levels)))))
        
    cbar.ax.tick_params(labelsize=cbar_tick_labelsize) 
    text = cbar.ax.yaxis.label
    font = matplotlib.font_manager.FontProperties(style=cbar_font_style, size=cbar_font_size,
                                                  stretch=cbar_font_stretch)
    text.set_font_properties(font)
    cbar.set_label(cbar_label, labelpad=cbar_label_pad, y= cbar_label_y)

    pass
##########################
################
def plot_polygon():
    #### Draw 4 polygons to hide data outside of crystal lattice
    from matplotlib.patches import Polygon
    from matplotlib.collections import PatchCollection
    patches=[]
    #    invisibility=1
    #    polygon_color='white'
    global bottom_left,bottom_left_left,bottom_left_down,bottom_right,bottom_right_right,bottom_right_down
    global top_left,top_left_left,top_left_up, top_right,top_right_right,top_right_up
    bottom_left = [0,0]
    bottom_left_left=[bottom_left[0]-extent,bottom_left[1]]
    bottom_left_down=[bottom_left[0]-extent,bottom_left[1]-extent]
    bottom_right = [Transform_matrix[0][0],Transform_matrix[0][1]]
    
    bottom_right_right = [bottom_right[0]+extent,bottom_right[1]]
    bottom_right_down = [bottom_right[0]+extent,bottom_right[1]-extent]
    
    top_left = [Transform_matrix[1][0],Transform_matrix[1][1]]
    top_left_left = [top_left[0]-extent,top_left[1]]
    top_left_up = [top_left[0]-extent,top_left[1]+extent]
    top_right = [bottom_right[0] + top_left[0],bottom_right[1] + top_left[1]]
    top_right_right = [top_right[0]+extent,top_right[1]]
    top_right_up = [top_right[0]+extent,top_right[1]+extent]
    #
    #print 'draw_polygon =' + str(draw_polygon)
    if (draw_polygon==True):    
        #right 
        polygon = Polygon([top_right,top_right_right,bottom_right_right,bottom_right], True)
        patches.append(polygon)
        p = PatchCollection(patches, alpha=invisibility,color=polygon_color)
        ax.add_collection(p)
        #left
        polygon = Polygon([bottom_left,top_left,top_left_left,bottom_left_left], True)
        patches.append(polygon)
        p = PatchCollection(patches, alpha=invisibility,color=polygon_color)
        ax.add_collection(p)
        #bottom
        polygon = Polygon([bottom_left_left,bottom_left_down,bottom_right_down,bottom_right_right], True)
        patches.append(polygon)
        p = PatchCollection(patches, alpha=invisibility,color=polygon_color)
        ax.add_collection(p)
        #top
        polygon = Polygon([top_left_left,top_left_up,top_right_up,top_right_right], True)
        patches.append(polygon)
        p = PatchCollection(patches, alpha=invisibility,color=polygon_color)
        ax.add_collection(p)

    pass
################################
def boundary_atoms_adding():
    ## Add atoms at Boundaries
    global substrate
    tmp=round_coordinates(substrate)
    #sort atoms based on z value
    #substrate=ase.build.sort(substrate,substrate.get_positions()[:,2])
    add_atom_at_boundary(tmp)
    substrate=ase.build.sort(tmp,tmp.get_chemical_symbols())
    substrate.set_constraint(None)
       
    write(substrate_file + '_full.vasp',substrate,vasp5=True,direct=True)
    write(substrate_file + '_full.pdb',substrate,format='proteindatabank')
#    write(substrate_file + '_full.xyz',substrate,format='xyz')
    pass

def plot_atoms_plt(z_down,z_up,fill=False):
    global Atoms_X,Atoms_Y,Atoms_Z
    Atoms_X = substrate.get_positions()[:,0]
    Atoms_Y = substrate.get_positions()[:,1]
    Atoms_Z = substrate.get_positions()[:,2]

    for i in range(len(Atoms_X)):
       # draw only atoms between z_cut_downline and z_cut_upline
        if z_down<=Atoms_Z[i]<=z_up:
            circle=plt.Circle((Atoms_X[i],Atoms_Y[i]),r_circle,
                              fill=False,alpha=transparency,color=r_linecolor,zorder=1)
            ax.add_artist(circle)
            if fill==True:
                circle=plt.Circle((Atoms_X[i],Atoms_Y[i]),r_circle,
                              fill=True,alpha=transparency,color=r_linecolor,zorder=1)
                ax.add_artist(circle)
            # Draw upper layer
            if(Atoms_Z[i]>z_cutoff):
                circle=plt.Circle((Atoms_X[i],Atoms_Y[i]),r_circle,
                              fill=True,alpha=min(transparency*2,1),color=r_color,zorder=2)
                ax.add_artist(circle)
    pass

def plot_ase_style(z_down,z_up,zorder=10,alpha=0.7):
    global Atoms_X,Atoms_Y,Atoms_Z
    global ase_atom_alpha
    
    from ase.data import covalent_radii
    from ase.data.colors import jmol_colors
    atom_num  = substrate.get_atomic_numbers()
    radii = covalent_radii[atom_num]
    colors = jmol_colors[atom_num]
    for i in range(len(substrate)):
       # draw only atoms between z_cut_downline and z_cut_upline
        if z_down<=Atoms_Z[i]<=z_up:
            #print 'aha'
            circle=plt.Circle((Atoms_X[i],Atoms_Y[i]),radii[i],
                              fill=True,alpha=ase_atom_alpha,color=colors[i],zorder=zorder)
            ax.add_artist(circle)
           
            # Draw upper layer
            if(Atoms_Z[i]>z_cutoff):
                circle=plt.Circle((Atoms_X[i],Atoms_Y[i]),radii[i],
                              fill=True,alpha=min(ase_atom_alpha+0.2,1),color=colors[i],zorder=zorder)
                ax.add_artist(circle)
    pass
            
def plot_bonds(z_down,z_up):
    for i in range(len(Atoms_X)):
        for j in range(len(Atoms_X)):
            # draw only atoms between z_cut_downline and z_cut_upline
            if z_down<=Atoms_Z[i]<=z_up and z_down<=Atoms_Z[j]<=z_up:
                #Calculate distance between i-th atom and j-th atom
                d=np.sqrt((Atoms_X[i]-Atoms_X[j])**2 + (Atoms_Y[i]-Atoms_Y[j])**2 + (Atoms_Z[i]-Atoms_Z[j])**2)
                if d<d_cutoff:
                    # draw bond connecting from left to right
                    if(Atoms_X[i]<Atoms_X[j]):
                        Left_x,Right_x=Atoms_X[i], Atoms_X[j]
                        Left_y,Right_y=Atoms_Y[i], Atoms_Y[j]
                    elif (Atoms_X[i]>=Atoms_X[j]):
                        Left_x,Right_x=Atoms_X[j],Atoms_X[i]
                        Left_y,Right_y=Atoms_Y[j],Atoms_Y[i] 
                    # calculate angle of the line connecting the two points
                    theta = math.atan((Right_y-Left_y)/(Right_x-Left_x))
                    plt.plot([Left_x+r_line*math.cos(theta),Right_x-r_line*math.cos(theta)],
                              [Left_y+r_line*math.sin(theta),Right_y-r_line*math.sin(theta)],
                               'k-',alpha=transparency,color=r_bondcolor,zorder=5)
    pass

def plot_finalize():              
    xmin = min([bottom_left[0],top_left[0]]) - left_margin 
    xmax = max([bottom_right[0],top_right[0]]) + right_margin
    ymin = min([bottom_left[1],bottom_right[1]]) - bottom_margin
    ymax = max([top_left[1],top_right[1]]) + top_margin
    
    plt.title(plt_title)
    #plt.xlabel(r'X ($\AA$)')
    #plt.ylabel(r'Y ($\AA$)') 
    
    plt.xlabel(plt_xlabel)
    plt.ylabel(plt_ylabel)  
              
    plt.xlim([xmin,xmax])
    ax.xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(5))
    ax.xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(1))
    plt.ylim([ymin,ymax])
    ax.yaxis.set_major_locator(matplotlib.ticker.MultipleLocator(5))
    ax.yaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(1))
    
    #text = ax.yaxis.label
    #font = matplotlib.font_manager.FontProperties(style='italic', size=16,stretch='expanded')
    #text.set_font_properties(font)
    
    plt.minorticks_off()

    pass
####################################################
def main_prog():
    load_substrate()
    load_energy()
    replicate_data()
    transform_data()
    plot_heatmap()
    plot_polygon()
    boundary_atoms_adding()
    pass

###############################################################
    #####################################################
    ######################################################
## MAIN PROGRAM STARTS HERE
# Main program - initialization here
# Hidden function: python PES_5.py POSCAR.vasp
# to plot POSCAR file!!!
# Check for substrate argument
if len(sys.argv)>1:
    print( 'POSCAR structure plotting function ...')
    load_config_file()
    # Override substrate_file
    global substrate_file
    substrate_file = sys.argv[1]
    plt_filename1 = sys.argv[1] + '_1.png'
    plt_filename2 = sys.argv[1] + '_2.png'
    plt_title = ''
    load_substrate()
    boundary_atoms_adding()
    
    fig,ax=plt.subplots()
    ax.set_aspect('equal')
    ax.set_facecolor(ase_background_color)
    ax.set_alpha(ase_background_alpha)
    plot_atoms_plt(z_cut_downline,z_cut_upline)
    plot_bonds(z_cut_downline,z_cut_upline)
    plot_polygon()
    plot_finalize()
    print( 'Printing plot to: ' + plt_filename1 + '...')
    plt.savefig(plt_filename1,dpi=plt_dpi,transparent=True,bbox_inches='tight')
    bottom_z,top_z=3,10
    plot_ase_style(bottom_z,top_z,zorder=20)
    plot_finalize()

    print( 'Printing plot to: ' + plt_filename2 + '...')
    plt.savefig(plt_filename2,dpi=plt_dpi,transparent=True,bbox_inches='tight')
    plt.close()
    
    sys.exit(1)
    
# CLear screen
print('\033[H\033[J')
print( 'PES program developed by Pham Trong Lam phamtronglam2001@gmail.com')
print( 'Projected Potential Energy Surface and 2D lattice plot - verion ', pes_version)
print( 'Program started at: ' + str(datetime.now()))
print( "IF ERROR OCCURS AND YOU DON'T KNOW WHAT ELSE TO DO, THEN DELETE " + configfile_name + " AND RERUN THE PROGRAM!")
print( '\n\n')


load_config_file()
print( 'data_file = ',data_file)
print( 'xyzcolumns_index = ',xyzcolumns_index)
print( 'substrate_file = ', substrate_file)
print( 'cbar_level_override = ',cbar_level_override)
print( 'cbar_level_manual = ',cbar_level_manual)
print( 'draw_polygon =' + str(draw_polygon))
main_prog()
print( 'draw_by_ase = ',draw_by_ase)
plot_atoms_plt(z_cut_downline,z_cut_upline)
plot_bonds(z_cut_downline,z_cut_upline)

plot_finalize()
print( 'Printing plot to: ' + plt_filename + ' ...')
plt.savefig(plt_filename,dpi=plt_dpi,transparent=True,bbox_inches='tight')
plt.close()
if draw_by_ase==False:
    print( 'Plotted successfully!')
    print( 'Program ended at: ' + str(datetime.now()))
    sys.exit(1)
    
if draw_by_ase==True:
    
    #First ase figure with heatmap and adsorbate
    main_prog()
    plot_atoms_plt(z_cut_downline,z_cut_upline)
    plot_bonds(z_cut_downline,z_cut_upline)
    
    from ase.data import covalent_radii
    from ase.data.colors import jmol_colors
    atom_num  = substrate.get_atomic_numbers()
    radii = covalent_radii[atom_num]
    colors = jmol_colors[atom_num]
    bottom_z,top_z=3,10
    plot_ase_style(bottom_z,top_z)
    #plot_atoms(substrate,ax=ax,rotation=('0x,0y,0z'),show_unit_cell=True,offset=offset,radii=radii,colors=colors)

    plot_finalize()
    print( 'Printing plot to: ' + plt_filename + '.heatmap_adsorbate.png' + ' ...')
    plt.savefig(plt_filename + '.heatmap_adsorbate.png',dpi=plt_dpi,transparent=True,bbox_inches='tight')
    plt.close('all')
    
    # Second figure not heatmap (lattice and adsorbate)
    load_substrate()
    fig,ax=plt.subplots()
    ax.set_aspect('equal')
    ax.set_facecolor(ase_background_color)
    ax.set_alpha(ase_background_alpha)
    boundary_atoms_adding()
    
    plot_atoms_plt(z_cut_downline,z_cut_upline)
    plot_bonds(z_cut_downline,z_cut_upline)
    plot_ase_style(bottom_z,top_z,zorder=20)
    plot_polygon()
    
    plot_finalize()
    print( 'Printing plot to: ' + plt_filename + '.adsorbate.png' + ' ...')
    plt.savefig(plt_filename + '.adsorbate.png',dpi=plt_dpi,transparent=True,bbox_inches='tight')
    plt.close()
    
    # Third figure not heatmap (lattice only)
    load_substrate()
    fig,ax=plt.subplots()
    ax.set_aspect('equal')
    ax.set_facecolor(ase_background_color)
    ax.set_alpha(ase_background_alpha)
    boundary_atoms_adding()
    
    plot_atoms_plt(z_cut_downline,z_cut_upline,fill=True)
    plot_bonds(z_cut_downline,z_cut_upline)
    plot_polygon()
    
    plot_finalize()
    print( 'Printing plot to: ' + plt_filename + '.substrate.png' + ' ...')
    plt.savefig(plt_filename + '.substrate.png',dpi=plt_dpi,transparent=True,bbox_inches='tight')
    plt.close()
    
    # Fourth figure (ase style)
    fig,ax=plt.subplots()
    ax.set_aspect('equal')
    ax.set_facecolor(ase_background_color)
    ax.set_alpha(ase_background_alpha)
    plot_ase_style(z_cut_downline,z_cut_upline,alpha=ase_atom_alpha)
    plot_ase_style(bottom_z,top_z,zorder=30)
    plot_polygon()
    plot_finalize()
    print( 'Printing plot to: ' + plt_filename + '.ase_style.png' + ' ...')
    plt.savefig(plt_filename + '.ase_style.png',dpi=plt_dpi,transparent=True,bbox_inches='tight')
    plt.close()
    
    # Fifth figure (ase style substrate only)
    fig,ax=plt.subplots()
    ax.set_aspect('equal')
    ax.set_facecolor(ase_background_color)
    ax.set_alpha(ase_background_alpha)
    plot_ase_style(z_cut_downline,z_cut_upline,alpha=ase_atom_alpha)
    plot_polygon()
    plot_finalize()
    print( 'Printing plot to: ' + plt_filename + '.ase_style_substrate.png' + ' ...')
    plt.savefig(plt_filename + '.ase_style_substrate.png',dpi=plt_dpi,transparent=True,bbox_inches='tight')
    plt.close()
print( 'Plotted successfully!')
print( 'Program ended at: ' + str(datetime.now()))
sys.exit(1)