# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 09:34:36 2018

@author: lam
"""

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker
#from scipy.interpolate import griddata
import ase
from ase import Atoms
#from ase import Atom
from ase.io import read
import ase.build

#substrate_file='POSCAR_z1.vasp'
#substrate=ase.build.sort(read(substrate_file))

def add_atom_at_boundary(substrate):
    #Append boundary atoms x+1, y+1
    boundary_cutoff_fractional_down=0.01
    boundary_cutoff_fractional_up=0.99
    cell_par=np.zeros((3,3))
    cell_par=substrate.get_cell()
    for i in range(len(substrate)):
        x = substrate.get_scaled_positions()[i][0]
        y = substrate.get_scaled_positions()[i][1]
        z = substrate.get_scaled_positions()[i][2]
        #print substrate.get_chemical_symbols()[i] + str(i), x, y ,z
        if x<boundary_cutoff_fractional_down:
            current_atom = substrate.get_chemical_symbols()[i]
            
            translated_atom = Atoms(current_atom,cell=cell_par,scaled_positions=[(x+1,y,z)])
            
            substrate.extend(translated_atom)
            #print 'Xdown', len(substrate)
            if y<boundary_cutoff_fractional_down:
                current_atom = substrate.get_chemical_symbols()[i]
                
                translated_atom = Atoms(current_atom,cell=cell_par,scaled_positions=[(x+1,y+1,z)])
                
                substrate.extend(translated_atom)
                #print 'X Ydown',len(substrate)
            if y>boundary_cutoff_fractional_up:
                current_atom = substrate.get_chemical_symbols()[i]
                
                translated_atom = Atoms(current_atom,cell=cell_par,scaled_positions=[(x+1,y-1,z)])
                
                substrate.extend(translated_atom)
                #print 'X Y Yup',len(substrate)                
        if y<boundary_cutoff_fractional_down:
            current_atom = substrate.get_chemical_symbols()[i]
            
            translated_atom = Atoms(current_atom,cell=cell_par,scaled_positions=[(x,y+1,z)])
            
            substrate.extend(translated_atom)
            #print 'Ydown', len(substrate)
        if x>boundary_cutoff_fractional_up:
            current_atom = substrate.get_chemical_symbols()[i]
            
            translated_atom = Atoms(current_atom,cell=cell_par,scaled_positions=[(x-1,y,z)])
            
            substrate.extend(translated_atom)
            #print 'Xup', len(substrate)
            if y>boundary_cutoff_fractional_up:
                current_atom = substrate.get_chemical_symbols()[i]
                
                translated_atom = Atoms(current_atom,cell=cell_par,scaled_positions=[(x-1,y-1,z)])
                
                substrate.extend(translated_atom)
                #print 'X Yup',len(substrate)
                
        if y>boundary_cutoff_fractional_up:
            current_atom = substrate.get_chemical_symbols()[i]
            
            translated_atom = Atoms(current_atom,cell=cell_par,scaled_positions=[(x,y-1,z)])
            
            substrate.extend(translated_atom)
            #print 'Yup', len(substrate)
    
    pass

def round_coord(frac_coord,threshold = 0.005):
    if abs(frac_coord-0.)<threshold:
        return 0
#        print('return 0')
    if abs(frac_coord-1.)<threshold:
        return 1
#        print('return 1')
    if abs(frac_coord+1.)<threshold:
        return -1
#        print ('return -1')
    else:
        return frac_coord
#        print('return original')
    pass

def round_coordinates(substrate):
    cell_par=np.zeros((3,3))
    cell_par=substrate.get_cell()
    n=len(substrate)
    tmp=Atoms(cell=cell_par)
    for i in range(n):
        x = substrate.get_scaled_positions()[i][0]
        y = substrate.get_scaled_positions()[i][1]
        z = substrate.get_scaled_positions()[i][2]
        current_atom = substrate.get_chemical_symbols()[i]
        round_off_atom = Atoms(current_atom,cell=cell_par,
                               scaled_positions=[(round_coord(x),
                                                  round_coord(y),
                                                  round_coord(z))])
#        TESTED but failed, try in future
#        del substrate[i]
#        substrate.extend(round_off_atom)
        tmp.extend(round_off_atom)    
    
    return tmp

#add_atom_at_boundary(substrate)
#print len(substrate)


#from ase.visualize.plot import plot_atoms
#print 'Plotting ....'
#plt.figure()
#ax = plt.gca()
#ax.set_aspect('equal')
#plot_atoms(substrate,ax=ax,rotation=('0x,0y,0z'),show_unit_cell=True)